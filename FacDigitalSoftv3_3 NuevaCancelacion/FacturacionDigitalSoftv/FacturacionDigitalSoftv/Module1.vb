﻿Imports System.Data.SqlClient
Imports System.Net.NetworkInformation
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Xml
Imports Microsoft
Imports System.Threading
Module Module1


    Public mIva As Decimal = 0
    Public mipes As Decimal = 0
    Public msubtotal As Decimal = 0
    Public mdetalle1 As String = Nothing
    Public Graba As String = "0"
    Public SumaSubtotalYIeps As Boolean = True

    Public Sub Llenalog(ByVal texto As String)
        Try
            If Graba = "1" Then
                texto = Now.ToString & " - " & texto
                'Dim fic As String = "\\192.168.1.230\exes\SOFTV\log" + Now.Year.ToString + Now.Month.ToString + Now.Day.ToString + ".txt"
                'Dim sw As System.IO.StreamWriter

                'Dim fileExists As Boolean
                'fileExists = My.Computer.FileSystem.FileExists(fic)
                'If fileExists = False Then
                '    sw = New System.IO.StreamWriter(fic)
                'Else
                '    sw = My.Computer.FileSystem.OpenTextFileWriter(fic, True)
                'End If

                'sw.WriteLine(texto)
                'sw.Close()
            End If
        Catch ex As Exception


        End Try

    End Sub

    Public Sub Dame_Detalle1Global(ByVal oClv_FacturaCFD As Long, ByVal locconexion As String)
        mIva = 0
        mipes = 0
        msubtotal = 0
        mdetalle1 = ""

        Dim con As New SqlConnection(locconexion)
        Dim com As New SqlCommand("Dame_Detalle1", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_FacturaCFD
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@Iva", SqlDbType.Money)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@Ieps", SqlDbType.Money)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Dim prm4 As New SqlParameter("@Subtotal", SqlDbType.Money)
        prm4.Direction = ParameterDirection.Output
        prm4.Value = ""
        com.Parameters.Add(prm4)

        Dim prm5 As New SqlParameter("@Detalle1", SqlDbType.VarChar, 8000)
        prm5.Direction = ParameterDirection.Output
        prm5.Value = ""
        com.Parameters.Add(prm5)

        Try
            con.Open()
            com.ExecuteNonQuery()
            mIva = prm2.Value
            mipes = prm3.Value
            msubtotal = prm4.Value
            mdetalle1 = prm5.Value

        Catch ex As Exception
            If Graba = "0" Then
                Llenalog(ex.Source.ToString & " " & ex.Message)
            Else

                Llenalog(ex.Source.ToString & " " & ex.Message)
            End If
        Finally
            con.Close()
        End Try
    End Sub
End Module
