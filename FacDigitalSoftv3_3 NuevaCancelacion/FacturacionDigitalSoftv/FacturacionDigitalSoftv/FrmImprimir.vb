﻿Imports MizarCFD.DAL
Imports MizarCFD.BRL
Imports ThoughtWorks.QRCode
Imports MizarCFD.Reportes
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Public Class FrmImprimir


    Private Sub IMPRIMIRglobal(ByVal oClv_Factura As Long, ByVal LocConexion As String)
        Try
            Dim oidcompania As Integer = 0

            'GloServerName = "TEAMEDGAR-PC"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"
            '' ''
            'MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;Persist Security Info=True;User ID=" & GloUserID & ";Password=" & GloPassword & ""
            'Mandamos a Llamar los Datos para la Factura Globla
            Dame_Detalle1Global(oClv_Factura, LocConexion)

            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")
                Dim oRreportes As New MizarCFD.Reportes.MizarCFDi
                Dim archivoCFD As String = Nothing
                Dim archivoPDF As String = Nothing
                Dim oCFD As New MizarCFD.BRL.CFD
                oCFD.IdCFD = CStr(oClv_Factura)
                ' If oCFD.Consultar(Cnx) = True Then


                oSerie = oCFD.Serie
                oidcompania = oCFD.IdCompania

                Dim document As New System.Xml.XmlDocument
                document = oCFD.ObtenerXML()
                document.Save(ClassCFDI.RutaReportes + "\abcglobal.xml")
                CadenaOriginal = oCFD.CadenaOriginal

                Cantidad_Con_Letra = ClassCFDI.Dame_Cantidad_Con_Letra(CDec(oCFD.Total), LocConexion)
                'End If
                'oRreportes.cfd.ReadXml("C:\MizarCFD\abc.xml")
            End Using
            ''Set RDL file. 
            '' Supply a DataTable corresponding to each report 
            '' data source. 
            Dim dataSet As New DataSet()
            dataSet.ReadXml(ClassCFDI.RutaReportes + "\abcglobal.xml")
            Dim colString As DataColumn = New DataColumn("Cadena_Original")
            colString.DataType = System.Type.GetType("System.String")
            dataSet.Tables("Comprobante").Columns.Add(colString)
            dataSet.Tables("Comprobante").Rows(0)("Cadena_Original") = CadenaOriginal

            Dim colString2 As DataColumn = New DataColumn("Cantidad_Con_Letra")
            colString2.DataType = System.Type.GetType("System.String")
            dataSet.Tables("Comprobante").Columns.Add(colString2)
            dataSet.Tables("Comprobante").Rows(0)("Cantidad_Con_Letra") = Cantidad_Con_Letra

            Dim colString3 As DataColumn = New DataColumn("Serie")
            colString3.DataType = System.Type.GetType("System.String")
            dataSet.Tables("Comprobante").Columns.Add(colString3)
            dataSet.Tables("Comprobante").Rows(0)("Serie") = oSerie

            Dim colString4 As DataColumn = New DataColumn("Iva")
            colString4.DataType = System.Type.GetType("System.Decimal")
            dataSet.Tables("Comprobante").Columns.Add(colString4)
            dataSet.Tables("Comprobante").Rows(0)("Iva") = mIva

            Dim colString5 As DataColumn = New DataColumn("Ieps")
            colString5.DataType = System.Type.GetType("System.Decimal")
            dataSet.Tables("Comprobante").Columns.Add(colString5)
            dataSet.Tables("Comprobante").Rows(0)("Ieps") = mipes + msubtotal

            Dim colString6 As DataColumn = New DataColumn("Detalle1")
            colString6.DataType = System.Type.GetType("System.String")
            dataSet.Tables("Comprobante").Columns.Add(colString6)
            dataSet.Tables("Comprobante").Rows(0)("Detalle1") = mdetalle1

            Dim Report As New ReportDocument


            If oidcompania = 1 Then
                Report.Load(ClassCFDI.RutaReportes + "\ReportFacturaDigitalGlobal.rpt")
            Else
                Report.Load(ClassCFDI.RutaReportes + "\ReportFacturaDigitalGlobalgrupo.rpt")
            End If


            'Report.SetDataSource(dataSet)
            ClassCFDI.SetDBReport(dataSet, Report)

            Dim r As New Globalization.CultureInfo("es-MX")

            r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

            System.Threading.Thread.CurrentThread.CurrentCulture = r
            'CrystalReportViewer1.ReportSource = Report

        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Source)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    'Public Sub qr()
    '    Dim a As ThoughtWorks.QRCode.Codec.Data.QRCodeBitmapImage
    'End Sub

    Public Sub IMPRIMIR(ByVal oClv_Factura As Long)

        Try
            Dim oidcompania As Integer = 0

            'GloServerName = "TEAMEDGAR-PC"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"
            '' ''
            'MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;Persist Security Info=True;User ID=" & GloUserID & ";Password=" & GloPassword & ""
            'oClv_Factura = 76

            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")
                'Dim oRreportes As New MizarCFD.Reportes.MizarCFDi
                'Dim archivoCFD As String = Nothing
                'Dim archivoPDF As String = Nothing
                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)
                oCFD.GenerarArchivosXMLPDF(Cnx)
                oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_pdf", "Id_cfd = " + CStr(oClv_Factura))




                Process.Start(oPath)
                Dim r As New Globalization.CultureInfo("es-MX")

                r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"

                System.Threading.Thread.CurrentThread.CurrentCulture = r
                'oreportes.ListaRDS_CFD(Cnx, CStr(oClv_Factura))
                'oCFD.Consultar(Cnx)
                'oSerie = oCFD.Serie
                'oidcompania = oCFD.IdCompania
                'Dim document As New System.Xml.XmlDocument
                'document = oEFAC.ObtenerXML(CStr(oClv_Factura)) 'oCFD.ObtenerXML()
                'document.Save(Class1.RutaReportes + "\MizarCFDi.xsd")
                'CadenaOriginal = oEFAC.ConsultarCadenaOriginal(CStr(oClv_Factura))


                'Cantidad_Con_Letra = Dame_Cantidad_Con_Letra(CDec(oCFD.Total))
                'oRreportes.cfd.ReadXml("C:\MizarCFD\abc.xml")

                ''Set RDL file. 
                '' Supply a DataTable corresponding to each report 
                '' data source. 



                'dataSet.ReadXml(Class1.RutaReportes + "\MizarCFDi.xsd")
                'Dim colString As DataColumn = New DataColumn("Cadena_Original")
                'colString.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString)
                'dataSet.Tables("Comprobante").Rows(0)("Cadena_Original") = CadenaOriginal

                'Dim colString2 As DataColumn = New DataColumn("Cantidad_Con_Letra")
                'colString2.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString2)
                'dataSet.Tables("Comprobante").Rows(0)("Cantidad_Con_Letra") = Cantidad_Con_Letra

                'Dim colString3 As DataColumn = New DataColumn("Serie")
                'colString3.DataType = System.Type.GetType("System.String")
                'dataSet.Tables("Comprobante").Columns.Add(colString3)
                'dataSet.Tables("Comprobante").Rows(0)("Serie") = oSerie

                'If (Not dataSet.Tables("Domicilio").Columns.Contains("localidad")) Then
                '    Dim colString4 As DataColumn = New DataColumn("localidad")
                '    colString4.DataType = System.Type.GetType("System.String")
                '    dataSet.Tables("Domicilio").Columns.Add(colString4)
                '    dataSet.Tables("Domicilio").Rows(0)("localidad") = ""
                'End If

                'Dim Report As New ReportDocument

                'If oidcompania = 1 Then
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                ' Else
                'Report.Load(Class1.RutaReportes + "\RFDigital2012.rpt")
                'End If

                'Report.SetDataSource(oreportes)

                'SetDBReport(dataSet, Report)

                'CrystalReportViewer1.ReportSource = Report
            End Using
            Me.Close()
            'GloClv_Factura = 0
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Source)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub generarXml(ByVal oClv_Factura As Long)
        Try
            Dim oidcompania As Integer = 0


            Dim CadenaOriginal As String = Nothing
            Dim oSerie As String = Nothing
            Dim Cantidad_Con_Letra As String = Nothing
            Using Cnx As New DAConexion("HL", "sa", "sa")

                Dim odas As New MizarCFD.DAL.DAUtileriasSQL
                Dim oCFD As New MizarCFD.BRL.CFD
                Dim oPath As String = ""
                oCFD.IdCFD = CStr(oClv_Factura)
                oCFD.IdCompania = "1"
                oCFD.Consultar(Cnx)
                oCFD.GenerarArchivosXMLPDF(Cnx)

                oPath = DAUtileriasSQL.ObtenerArchivo(Cnx, "cfd_archivos", "archivo_xml", "Id_cfd = " + CStr(oClv_Factura))
                Process.Start(oPath)

            End Using
            Me.Close()
            'ClassCFDI.GloClv_FacturaCFD = 0
        Catch ex As Exception
            'MsgBox(ex.Message & " " & ex.Source)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimir_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If ClassCFDI.Locop = 1 Then
            IMPRIMIR(ClassCFDI.GloClv_FacturaCFD)
        Else
            IMPRIMIR(ClassCFDI.GloClv_FacturaCFD)
        End If
    End Sub
End Class