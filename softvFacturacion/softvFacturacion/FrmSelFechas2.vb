Public Class FrmSelFechas2

    Private Sub FrmSelFechas2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        eBndMensAde = False
    End Sub

    Private Sub FrmSelFechas2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
        colorea(Me)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaInicial = Me.DateTimePicker1.Value
        eFechaFinal = Me.DateTimePicker2.Value
        If eBndMensAde = True Then
            eBndMensAde = False
            Dim imprimir As FrmImprimir = New FrmImprimir
            imprimir.ReporteMesesAdelantados(Me.DateTimePicker1.Value, Me.DateTimePicker2.Value)
            imprimir.Show()
            Me.Close()
        ElseIf rBndCelpromo = True Then
            rBndCelpromo = False
            Dim imprimir As FrmImprimir = New FrmImprimir
            imprimir.ReporteClientesCelpromo(Me.DateTimePicker1.Value, Me.DateTimePicker2.Value)
            imprimir.Show()
            Me.Close()
        Else
            FrmSelCiudad2.Show()
        End If

        Me.Close()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub
End Class