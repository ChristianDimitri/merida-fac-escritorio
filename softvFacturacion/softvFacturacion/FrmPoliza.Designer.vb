<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBClv_Llave_PolizaLabel As System.Windows.Forms.Label
        Dim CMBFechaLabel As System.Windows.Forms.Label
        Dim CMBClv_UsuarioLabel As System.Windows.Forms.Label
        Dim CMBStatusLabel As System.Windows.Forms.Label
        Dim CMBConceptoLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Consulta_Genera_PolizaDataGridView = New System.Windows.Forms.DataGridView()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_Llave_PolizaTextBox = New System.Windows.Forms.TextBox()
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.FechaCDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.FechaVDateTimePicker = New System.Windows.Forms.DateTimePicker()
        CMBClv_Llave_PolizaLabel = New System.Windows.Forms.Label()
        CMBFechaLabel = New System.Windows.Forms.Label()
        CMBClv_UsuarioLabel = New System.Windows.Forms.Label()
        CMBStatusLabel = New System.Windows.Forms.Label()
        CMBConceptoLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBClv_Llave_PolizaLabel
        '
        CMBClv_Llave_PolizaLabel.AutoSize = True
        CMBClv_Llave_PolizaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_Llave_PolizaLabel.Location = New System.Drawing.Point(123, 30)
        CMBClv_Llave_PolizaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBClv_Llave_PolizaLabel.Name = "CMBClv_Llave_PolizaLabel"
        CMBClv_Llave_PolizaLabel.Size = New System.Drawing.Size(114, 20)
        CMBClv_Llave_PolizaLabel.TabIndex = 7
        CMBClv_Llave_PolizaLabel.Text = "Clave Póliza"
        CMBClv_Llave_PolizaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBFechaLabel
        '
        CMBFechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBFechaLabel.Location = New System.Drawing.Point(907, 35)
        CMBFechaLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBFechaLabel.Name = "CMBFechaLabel"
        CMBFechaLabel.Size = New System.Drawing.Size(202, 26)
        CMBFechaLabel.TabIndex = 9
        CMBFechaLabel.Text = "Fecha Documento :"
        CMBFechaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        AddHandler CMBFechaLabel.Click, AddressOf Me.CMBFechaLabel_Click
        '
        'CMBClv_UsuarioLabel
        '
        CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_UsuarioLabel.Location = New System.Drawing.Point(292, 27)
        CMBClv_UsuarioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        CMBClv_UsuarioLabel.Size = New System.Drawing.Size(433, 26)
        CMBClv_UsuarioLabel.TabIndex = 11
        CMBClv_UsuarioLabel.Text = "Usuario"
        CMBClv_UsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBStatusLabel
        '
        CMBStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBStatusLabel.Location = New System.Drawing.Point(733, 33)
        CMBStatusLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBStatusLabel.Name = "CMBStatusLabel"
        CMBStatusLabel.Size = New System.Drawing.Size(128, 20)
        CMBStatusLabel.TabIndex = 15
        CMBStatusLabel.Text = "Status"
        CMBStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBConceptoLabel
        '
        CMBConceptoLabel.AutoSize = True
        CMBConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBConceptoLabel.Location = New System.Drawing.Point(108, 105)
        CMBConceptoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CMBConceptoLabel.Name = "CMBConceptoLabel"
        CMBConceptoLabel.Size = New System.Drawing.Size(94, 20)
        CMBConceptoLabel.TabIndex = 17
        CMBConceptoLabel.Text = "Concepto:"
        '
        'Label1
        '
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(892, 65)
        Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(217, 26)
        Label1.TabIndex = 24
        Label1.Text = "Fecha de Contabilizacion :"
        Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.Location = New System.Drawing.Point(889, 95)
        Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(220, 26)
        Label3.TabIndex = 28
        Label3.Text = "Fecha Vencimiento :"
        Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Consulta_Genera_PolizaDataGridView
        '
        Me.Consulta_Genera_PolizaDataGridView.AllowUserToAddRows = False
        Me.Consulta_Genera_PolizaDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Genera_PolizaDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Genera_PolizaDataGridView.ColumnHeadersHeight = 35
        Me.Consulta_Genera_PolizaDataGridView.Location = New System.Drawing.Point(29, 178)
        Me.Consulta_Genera_PolizaDataGridView.Margin = New System.Windows.Forms.Padding(4)
        Me.Consulta_Genera_PolizaDataGridView.Name = "Consulta_Genera_PolizaDataGridView"
        Me.Consulta_Genera_PolizaDataGridView.Size = New System.Drawing.Size(1247, 518)
        Me.Consulta_Genera_PolizaDataGridView.TabIndex = 2
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(1247, 12)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(29, 197)
        Me.ToolStripContainer1.Margin = New System.Windows.Forms.Padding(4)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(1247, 37)
        Me.ToolStripContainer1.TabIndex = 3
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button5.Location = New System.Drawing.Point(1096, 768)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(180, 44)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(907, 768)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(180, 44)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Clv_Llave_PolizaTextBox
        '
        Me.Clv_Llave_PolizaTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_Llave_PolizaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Llave_PolizaTextBox.Location = New System.Drawing.Point(112, 972)
        Me.Clv_Llave_PolizaTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.Clv_Llave_PolizaTextBox.Name = "Clv_Llave_PolizaTextBox"
        Me.Clv_Llave_PolizaTextBox.ReadOnly = True
        Me.Clv_Llave_PolizaTextBox.Size = New System.Drawing.Size(121, 26)
        Me.Clv_Llave_PolizaTextBox.TabIndex = 8
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.Enabled = False
        Me.FechaDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(1117, 39)
        Me.FechaDateTimePicker.Margin = New System.Windows.Forms.Padding(4)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(141, 22)
        Me.FechaDateTimePicker.TabIndex = 10
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(292, 57)
        Me.Clv_UsuarioTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.ReadOnly = True
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(113, 26)
        Me.Clv_UsuarioTextBox.TabIndex = 12
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BackColor = System.Drawing.Color.White
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(415, 57)
        Me.NombreTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.ReadOnly = True
        Me.NombreTextBox.Size = New System.Drawing.Size(310, 26)
        Me.NombreTextBox.TabIndex = 14
        '
        'StatusTextBox
        '
        Me.StatusTextBox.BackColor = System.Drawing.Color.White
        Me.StatusTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox.Location = New System.Drawing.Point(733, 57)
        Me.StatusTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.ReadOnly = True
        Me.StatusTextBox.Size = New System.Drawing.Size(128, 26)
        Me.StatusTextBox.TabIndex = 16
        Me.StatusTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ConceptoTextBox
        '
        Me.ConceptoTextBox.BackColor = System.Drawing.Color.White
        Me.ConceptoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoTextBox.Location = New System.Drawing.Point(220, 105)
        Me.ConceptoTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.ConceptoTextBox.Multiline = True
        Me.ConceptoTextBox.Name = "ConceptoTextBox"
        Me.ConceptoTextBox.Size = New System.Drawing.Size(641, 66)
        Me.ConceptoTextBox.TabIndex = 18
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Red
        Me.TextBox1.Location = New System.Drawing.Point(797, 704)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(217, 29)
        Me.TextBox1.TabIndex = 19
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Red
        Me.TextBox2.Location = New System.Drawing.Point(1024, 704)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(251, 29)
        Me.TextBox2.TabIndex = 20
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(597, 704)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(192, 32)
        Me.CMBLabel1.TabIndex = 21
        Me.CMBLabel1.Text = "SUMAS IGUALES : "
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.White
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(127, 57)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(121, 26)
        Me.TextBox3.TabIndex = 22
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button2.Location = New System.Drawing.Point(503, 769)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(396, 44)
        Me.Button2.TabIndex = 23
        Me.Button2.Text = "EXPORTAR CONTPAQ"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FechaCDateTimePicker
        '
        Me.FechaCDateTimePicker.Enabled = False
        Me.FechaCDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaCDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaCDateTimePicker.Location = New System.Drawing.Point(1117, 69)
        Me.FechaCDateTimePicker.Margin = New System.Windows.Forms.Padding(4)
        Me.FechaCDateTimePicker.Name = "FechaCDateTimePicker"
        Me.FechaCDateTimePicker.Size = New System.Drawing.Size(141, 22)
        Me.FechaCDateTimePicker.TabIndex = 25
        '
        'FechaVDateTimePicker
        '
        Me.FechaVDateTimePicker.Enabled = False
        Me.FechaVDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaVDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaVDateTimePicker.Location = New System.Drawing.Point(1117, 99)
        Me.FechaVDateTimePicker.Margin = New System.Windows.Forms.Padding(4)
        Me.FechaVDateTimePicker.Name = "FechaVDateTimePicker"
        Me.FechaVDateTimePicker.Size = New System.Drawing.Size(141, 22)
        Me.FechaVDateTimePicker.TabIndex = 29
        '
        'FrmPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1303, 828)
        Me.Controls.Add(Me.FechaVDateTimePicker)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.FechaCDateTimePicker)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Consulta_Genera_PolizaDataGridView)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(CMBClv_Llave_PolizaLabel)
        Me.Controls.Add(Me.Clv_Llave_PolizaTextBox)
        Me.Controls.Add(CMBFechaLabel)
        Me.Controls.Add(Me.FechaDateTimePicker)
        Me.Controls.Add(CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(CMBStatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(CMBConceptoLabel)
        Me.Controls.Add(Me.ConceptoTextBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "FrmPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Captura de la Póliza"
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Consulta_Genera_PolizaDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Clv_Llave_PolizaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FechaCDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents FechaVDateTimePicker As System.Windows.Forms.DateTimePicker
End Class
