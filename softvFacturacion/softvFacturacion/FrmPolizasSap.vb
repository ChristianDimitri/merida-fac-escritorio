﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
'Imports SAPbobsCOM
Imports System.Data.SqlClient

Public Class FrmPolizasSap
    Public GloPlaza As String
    Private Sub LlenaMaster()

        BaseII.limpiaParametros()

        Me.DataGridViewMaster.DataSource = BaseII.ConsultaDT("Sp_MuestraMasterPolizaSap")
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
    End Sub

    Private Sub LlenaDetalle(LocIdPoliza As Long)
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (INICIO)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdPoliza", SqlDbType.BigInt, LocIdPoliza)
        Me.DataGridViewDetalle.DataSource = BaseII.ConsultaDT("Sp_MuestraDetallePolizaSap")
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
        LlenaTotalDetalle(LocIdPoliza)
    End Sub

    Private Sub LlenaTotalDetalle(LocIdPoliza As Long)
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (INICIO)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdPoliza", SqlDbType.BigInt, LocIdPoliza)
        Me.DataGridViewTotal.DataSource = BaseII.ConsultaDT("Sp_TotalMuestraDetallePolizaSap")
        '''''LLENAMOS EL GRID CON LA INFORMACIÓN CORRESPONDIENTE (FIN)
    End Sub

    Private Sub FrmPolizasSap_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        LlenaMaster()
        GloPlaza = SP_DameTblPlaza()
        Try
            LlenaDetalle(DataGridViewMaster.SelectedCells(0).Value)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ButtonSalir_Click(sender As Object, e As EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBuscaDescripcion_Click(sender As Object, e As EventArgs) Handles btnBuscaDescripcion.Click
        Dim frmfec As New FrmSelFechaNew
        Dim oResul As New DialogResult
        oResul = frmfec.ShowDialog()
        If oResul = Windows.Forms.DialogResult.OK Then
            MsgBox(GeneraPolizaNuevo_SAP(GloFechaSAP, GloUsuario))
            LlenaMaster()
        End If
    End Sub

    Public Function GeneraPolizaNuevo_SAP(LocFecha As String, LocClvUsuario As String) As String
        Try
            GeneraPolizaNuevo_SAP = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FechaSap", SqlDbType.VarChar, LocFecha, 10)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.VarChar, LocClvUsuario, 10)
            BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
            BaseII.ProcedimientoOutPut("GeneraPolizaNuevo_SAP")
            GeneraPolizaNuevo_SAP = BaseII.dicoPar("@Msj").ToString
        Catch ex As Exception
            GeneraPolizaNuevo_SAP = ""
        End Try
    End Function

    Public Function Sp_CancelaMasterPolizaSap(LocIdPoliza As Long, LocClvUsuario As String) As String
        Try
            Sp_CancelaMasterPolizaSap = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdPoliza", SqlDbType.Int, LocIdPoliza)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.VarChar, LocClvUsuario, 10)
            BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 400)
            BaseII.ProcedimientoOutPut("Sp_CancelaMasterPolizaSap")
            Sp_CancelaMasterPolizaSap = BaseII.dicoPar("@Msj").ToString
        Catch ex As Exception
            Sp_CancelaMasterPolizaSap = ""

        End Try
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Sp_CancelaMasterPolizaSap(DataGridViewMaster.SelectedCells(0).Value, GloUsuario)
            LlenaMaster()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridViewDetalle_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewDetalle.CellContentClick

    End Sub

    Private Sub DataGridViewDetalle_CurrentCellChanged(sender As Object, e As EventArgs) Handles DataGridViewDetalle.CurrentCellChanged

    End Sub

    Private Sub DataGridViewMaster_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewMaster.CellContentClick

    End Sub

    Private Sub DataGridViewMaster_CurrentCellChanged(sender As Object, e As EventArgs) Handles DataGridViewMaster.CurrentCellChanged
        Try


            LlenaDetalle(DataGridViewMaster.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub uspReportePolizaNew(ByVal LocIdPoliza As Long)
        '''''MANDAMOS EL REPORTE (INICIO)
        ControlEfectivoClass.limpiaParametros() ''LIMPIAMOS LA LISTA DE PARÁMETROS

        ControlEfectivoClass.CreateMyParameter("@IdPoliza", SqlDbType.BigInt, LocIdPoliza) ''MANDAMOS LOS PARÁMETROS

        Dim listaTablas As New List(Of String) ''MANDAMOS EL NOMBRE DE LAS TABLAS QUE DEVOLVERÁ EL DATASET DEL REPORTE
        listaTablas.Add("MasterPolizaSap")
        listaTablas.Add("DetallePolizaSap")

        Dim DataSet As New DataSet ''LLENAMOS EL DATASET QUE LLENARÁ EL REPORTE
        DataSet = ControlEfectivoClass.ConsultaDS("Sp_ReportePolizaNew", listaTablas)

        'Dim diccioFormulasReporte As New Dictionary(Of String, String) ''LENAMOS EL DICCIONARIO QUE CONTENDRÁ LAS FÓRMULAS QUE REQUIERE EL REPORTE
        'diccioFormulasReporte.Add("Ciudad", LocNomEmpresa)
        'diccioFormulasReporte.Add("Empresa", GloNomSucursal)

        'ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/rptEntregasGlobalesNuevas", DataSet, diccioFormulasReporte) ''MANDAMOS LLAMAR EL REPORTE
        ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/ReportPolizasNew", DataSet) ''MANDAMOS LLAMAR EL REPORTE
        '''''MANDAMOS EL REPORTE (INICIO)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            uspReportePolizaNew(DataGridViewMaster.SelectedCells(0).Value)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'Private Sub Proceso_SubirSap(LocIdpolizasap As Long)
    '    Dim ErrMsg As String = Nothing
    '    Dim ErrCode As Integer = 0
    '    Dim ResultConexion As Integer = 0
    '    Dim _count As Integer = 0





    '    Dim OCom As SAPbobsCOM.Company

    '    OCom = New SAPbobsCOM.Company

    '    OCom.Server = "SAPTELECABLE"
    '    OCom.CompanyDB = "SapTeleCable"
    '    OCom.DbUserName = "sa"
    '    OCom.DbPassword = "*Adminsys"
    '    OCom.LicenseServer = "SAPTELECABLE:30000"
    '    OCom.UserName = "manager"
    '    OCom.Password = "*Adminsys"
    '    OCom.UseTrusted = False
    '    OCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014
    '    ResultConexion = OCom.Connect

    '    If ResultConexion <> 0 Then

    '        OCom.GetLastError(ErrCode, ErrMsg)
    '        'TextBox1.Text = TextBox1.Text + ErrMsg + " " + CStr(ErrCode)
    '        MsgBox(ErrMsg + " " + CStr(ErrCode))
    '    Else
    '        'MsgBox("Conexion lista")
    '        'TextBox1.Text = TextBox1.Text + "Conexion lista"

    '        'Dim _Company As New SAPbobsCOM.Company
    '        Dim _AsientosAp As SAPbobsCOM.JournalEntries
    '        _AsientosAp = OCom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)


    '        Dim CON As New SqlConnection(MiConexion)
    '        Dim cmd As New SqlCommand()
    '        Try
    '            cmd = New SqlCommand()
    '            CON.Open()
    '            With cmd
    '                'Consulta_Tbl_Clv_Ref_Logi (@op int)	
    '                .CommandText = "Sp_APasarDetallePolizaSap"
    '                .Connection = CON
    '                .CommandTimeout = 0
    '                .CommandType = CommandType.StoredProcedure

    '                Dim prm As New SqlParameter("@IdPoliza", SqlDbType.Int)
    '                prm.Direction = ParameterDirection.Input
    '                prm.Value = LocIdpolizasap
    '                .Parameters.Add(prm)
    '                'TextBox1.Text = ""
    '                Dim reader As SqlDataReader = .ExecuteReader()
    '                While (reader.Read)

    '                    If _count = 0 Then
    '                        _AsientosAp.Memo = reader.GetValue(15).ToString
    '                        _AsientosAp.DueDate = reader.GetValue(13).ToString
    '                        _AsientosAp.ReferenceDate = reader.GetValue(12).ToString
    '                        _AsientosAp.TaxDate = reader.GetValue(14).ToString
    '                        _AsientosAp.Indicator = "01" 'reader.GetValue(10).ToString
    '                    End If


    '                    _AsientosAp.Lines.AccountCode = reader.GetValue(0).ToString
    '                    _AsientosAp.Lines.Debit = reader.GetValue(2).ToString
    '                    _AsientosAp.Lines.Credit = reader.GetValue(3).ToString
    '                    _AsientosAp.Lines.FederalTaxID = reader.GetValue(6).ToString
    '                    _AsientosAp.Lines.Reference2 = reader.GetValue(8).ToString
    '                    _AsientosAp.Lines.AdditionalReference = reader.GetValue(7).ToString
    '                    _AsientosAp.Lines.Reference1 = reader.GetValue(9).ToString
    '                    _AsientosAp.Lines.CostingCode = reader.GetValue(11).ToString
    '                    _AsientosAp.Lines.CostingCode2 = reader.GetValue(10).ToString

    '                    _AsientosAp.Lines.Add()
    '                    _count = _count + 1
    '                End While
    '            End With
    '            CON.Close()
    '        Catch ex As Exception
    '            If CON.State = ConnectionState.Open Then
    '                CON.Close()
    '            End If
    '            System.Windows.Forms.MessageBox.Show(ex.Message)
    '        End Try



    '        _AsientosAp.Lines.SetCurrentLine(_count - 1)

    '        Dim result As Integer = 0

    '        result = _AsientosAp.Add


    '        If result <> 0 Then

    '            OCom.GetLastError(ErrCode, ErrMsg)
    '            MsgBox(ErrMsg + " " + CStr(ErrCode))
    '        Else
    '            MsgBox("Se grabo con exito")
    '        End If

    '        If OCom.Connected = True Then
    '            If OCom.InTransaction = True Then
    '                OCom.EndTransaction(BoWfTransOpt.wf_Commit)
    '            End If
    '            OCom.Disconnect()
    '        End If
    '    End If

    'End Sub

    Public Function SP_DameTblPlaza() As String
        Try
            SP_DameTblPlaza = ""
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Plaza", ParameterDirection.Output, SqlDbType.VarChar, 150)
            BaseII.ProcedimientoOutPut("SP_DameTblPlaza")
            SP_DameTblPlaza = BaseII.dicoPar("@Plaza").ToString
        Catch ex As Exception
            SP_DameTblPlaza = ""
        End Try
    End Function

    Public Sub SP_INSERTATblTraspasaPolizas(LocIdpolizasap As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IdPoliza", SqlDbType.BigInt, LocIdpolizasap)
            BaseII.CreateMyParameter("@Plaza", SqlDbType.VarChar, GloPlaza, 150)
            BaseII.ProcedimientoOutPut("SP_INSERTATblTraspasaPolizas")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            'Proceso_SubirSap(DataGridViewMaster.SelectedCells(0).Value)
            SP_INSERTATblTraspasaPolizas(DataGridViewMaster.SelectedCells(0).Value)
            MsgBox("Se grabó correctamente su solicitud", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class