﻿Imports System.Data.SqlClient

Public Class BrwArtDinercap

    Public Sub Add_TblAdeudoDetalle(ByVal ClvSession As Integer, ByVal ClvCablemodem As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_AddArtListDinercap", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@Clv_Session", SqlDbType.Int)
            par1.Direction = ParameterDirection.Input
            par1.Value = ClvSession
            comando.Parameters.Add(par1)

            Dim par2 As New SqlParameter("@Clv_Cablemodem", SqlDbType.Int)
            par2.Direction = ParameterDirection.Input
            par2.Value = ClvCablemodem
            comando.Parameters.Add(par2)

            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Buscar(ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Try
            If Op = 0 Then

                Sp_Busca("", "", 0)

            ElseIf Op = 1 Then

                If (BSerie.Text) <> "" Then
                    Sp_Busca(BSerie.Text, "", 1)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If

            ElseIf Op = 2 Then

                If (BDesc.Text) <> "" Then
                    Sp_Busca("", BDesc.Text, 2)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If
            End If

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Sp_Busca(ByVal Serie As String, ByVal Nombre As String, ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try

            If Len(Serie) = 0 Then
                Serie = "''"
            ElseIf Len(Serie) > 0 Then
                Serie = "'" + Serie + "'"
            End If
            If Len(Nombre) = 0 Then
                Nombre = "''"
            ElseIf Len(Nombre) > 0 Then
                Nombre = "'" + Nombre + "'"
            End If

            consulta = "Exec Sp_AgregarListaDinercap " + CStr(Serie) + "," + CStr(Nombre) + "," + CStr(Op)

            CMD = New SqlDataAdapter(consulta, conexion)
            Dim dt As New DataTable
            Dim BS As New BindingSource

            CMD.Fill(dt)
            BS.DataSource = dt
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                Me.LblSerie.Text = CStr(Me.DataGridView1.SelectedCells(1).Value)
                Me.LblDescripcion.Text = (Me.DataGridView1.SelectedCells(2).Value)
            Catch ex As System.Exception

            End Try

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub BrwArtDinercap_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Buscar(0)
    End Sub

    Private Sub BtnSerie_Click(sender As System.Object, e As System.EventArgs) Handles BtnSerie.Click
        Me.Buscar(1)
        BSerie.Clear()
    End Sub

    Private Sub BtnDesc_Click(sender As System.Object, e As System.EventArgs) Handles BtnDesc.Click
        Me.Buscar(2)
        BDesc.Clear()
    End Sub

    Private Sub BntSalir_Click(sender As System.Object, e As System.EventArgs) Handles BntSalir.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub Agregar()
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Dim ClvCableModem As Integer
        If DataGridView1.RowCount > 0 Then
            ClvCableModem = Me.DataGridView1.SelectedCells(0).Value
            Add_TblAdeudoDetalle(gloClv_Session, ClvCableModem)
        Else
            MsgBox("Seleccione un registro para poder agregar a la lista ", MsgBoxStyle.Information)
        End If

        Me.Close()
    End Sub

    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        Agregar()
    End Sub

    Private Sub DataGridView1_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Dim i As Integer
        i = DataGridView1.CurrentRow.Index

        Me.LblSerie.Text = CStr(Me.DataGridView1.Item(1, i).Value())
        Me.LblDescripcion.Text = (Me.DataGridView1.Item(2, i).Value())

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentDoubleClick
        Agregar()
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        'preubas
        Agregar()
    End Sub

    '04/04/2018
End Class