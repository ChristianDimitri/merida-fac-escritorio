﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPolizasSap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridViewMaster = New System.Windows.Forms.DataGridView()
        Me.DataGridViewDetalle = New System.Windows.Forms.DataGridView()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.CMBlblEstatus = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnBuscaDescripcion = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.DataGridViewTotal = New System.Windows.Forms.DataGridView()
        CType(Me.DataGridViewMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridViewMaster
        '
        Me.DataGridViewMaster.AllowUserToAddRows = False
        Me.DataGridViewMaster.AllowUserToDeleteRows = False
        Me.DataGridViewMaster.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders
        Me.DataGridViewMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewMaster.Location = New System.Drawing.Point(12, 42)
        Me.DataGridViewMaster.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.DataGridViewMaster.Name = "DataGridViewMaster"
        Me.DataGridViewMaster.ReadOnly = True
        Me.DataGridViewMaster.RowTemplate.Height = 24
        Me.DataGridViewMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewMaster.Size = New System.Drawing.Size(827, 164)
        Me.DataGridViewMaster.TabIndex = 0
        '
        'DataGridViewDetalle
        '
        Me.DataGridViewDetalle.AllowUserToAddRows = False
        Me.DataGridViewDetalle.AllowUserToDeleteRows = False
        Me.DataGridViewDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewDetalle.Location = New System.Drawing.Point(12, 252)
        Me.DataGridViewDetalle.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.DataGridViewDetalle.Name = "DataGridViewDetalle"
        Me.DataGridViewDetalle.ReadOnly = True
        Me.DataGridViewDetalle.RowTemplate.Height = 24
        Me.DataGridViewDetalle.Size = New System.Drawing.Size(981, 412)
        Me.DataGridViewDetalle.TabIndex = 1
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(861, 671)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(132, 37)
        Me.ButtonSalir.TabIndex = 12
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'CMBlblEstatus
        '
        Me.CMBlblEstatus.AutoSize = True
        Me.CMBlblEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblEstatus.Location = New System.Drawing.Point(13, 18)
        Me.CMBlblEstatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblEstatus.Name = "CMBlblEstatus"
        Me.CMBlblEstatus.Size = New System.Drawing.Size(218, 20)
        Me.CMBlblEstatus.TabIndex = 28
        Me.CMBlblEstatus.Text = "Polizas SAP generadas :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 229)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(186, 20)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Detalle de la Poliza :"
        '
        'btnBuscaDescripcion
        '
        Me.btnBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaDescripcion.Location = New System.Drawing.Point(845, 42)
        Me.btnBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaDescripcion.Name = "btnBuscaDescripcion"
        Me.btnBuscaDescripcion.Size = New System.Drawing.Size(147, 31)
        Me.btnBuscaDescripcion.TabIndex = 30
        Me.btnBuscaDescripcion.Text = "&Generar Poliza"
        Me.btnBuscaDescripcion.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(845, 81)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(147, 31)
        Me.Button2.TabIndex = 32
        Me.Button2.Text = "&Cancelar Poliza"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(845, 121)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(147, 31)
        Me.Button3.TabIndex = 33
        Me.Button3.Text = "&Imprimir Poliza"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(845, 159)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(147, 47)
        Me.Button4.TabIndex = 34
        Me.Button4.Text = "&Subir Poliza a SAP"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'DataGridViewTotal
        '
        Me.DataGridViewTotal.AllowUserToAddRows = False
        Me.DataGridViewTotal.AllowUserToDeleteRows = False
        Me.DataGridViewTotal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders
        Me.DataGridViewTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewTotal.Location = New System.Drawing.Point(283, 638)
        Me.DataGridViewTotal.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.DataGridViewTotal.Name = "DataGridViewTotal"
        Me.DataGridViewTotal.ReadOnly = True
        Me.DataGridViewTotal.RowTemplate.Height = 24
        Me.DataGridViewTotal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewTotal.Size = New System.Drawing.Size(329, 71)
        Me.DataGridViewTotal.TabIndex = 35
        '
        'FrmPolizasSap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1005, 721)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnBuscaDescripcion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CMBlblEstatus)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.DataGridViewDetalle)
        Me.Controls.Add(Me.DataGridViewMaster)
        Me.Controls.Add(Me.DataGridViewTotal)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.Name = "FrmPolizasSap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Polizas SAP"
        CType(Me.DataGridViewMaster, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridViewMaster As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents CMBlblEstatus As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnBuscaDescripcion As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTotal As System.Windows.Forms.DataGridView
End Class
