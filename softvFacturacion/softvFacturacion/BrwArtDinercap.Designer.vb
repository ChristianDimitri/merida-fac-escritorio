﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwArtDinercap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LblDescripcion = New System.Windows.Forms.Label()
        Me.LabelDesc = New System.Windows.Forms.Label()
        Me.LabelSerie = New System.Windows.Forms.Label()
        Me.LblSerie = New System.Windows.Forms.Label()
        Me.BSerie = New System.Windows.Forms.TextBox()
        Me.LnlSerie = New System.Windows.Forms.Label()
        Me.BtnDesc = New System.Windows.Forms.Button()
        Me.BtnSerie = New System.Windows.Forms.Button()
        Me.BDesc = New System.Windows.Forms.TextBox()
        Me.LblDesc = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_CableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BntSalir = New System.Windows.Forms.Button()
        Me.BtnAgregar = New System.Windows.Forms.Button()
        Me.LblUserSis = New System.Windows.Forms.Label()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(842, 570)
        Me.Panel1.TabIndex = 3
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BSerie)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LnlSerie)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BtnDesc)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BtnSerie)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BDesc)
        Me.SplitContainer1.Panel1.Controls.Add(Me.LblDesc)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(842, 570)
        Me.SplitContainer1.SplitterDistance = 295
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.LblDescripcion)
        Me.GroupBox1.Controls.Add(Me.LabelDesc)
        Me.GroupBox1.Controls.Add(Me.LabelSerie)
        Me.GroupBox1.Controls.Add(Me.LblSerie)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(3, 249)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(286, 226)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Artículo"
        '
        'LblDescripcion
        '
        Me.LblDescripcion.AutoEllipsis = True
        Me.LblDescripcion.BackColor = System.Drawing.Color.DarkOrange
        Me.LblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDescripcion.Location = New System.Drawing.Point(82, 77)
        Me.LblDescripcion.Name = "LblDescripcion"
        Me.LblDescripcion.Size = New System.Drawing.Size(198, 146)
        Me.LblDescripcion.TabIndex = 15
        '
        'LabelDesc
        '
        Me.LabelDesc.AutoSize = True
        Me.LabelDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDesc.Location = New System.Drawing.Point(10, 77)
        Me.LabelDesc.Name = "LabelDesc"
        Me.LabelDesc.Size = New System.Drawing.Size(66, 13)
        Me.LabelDesc.TabIndex = 14
        Me.LabelDesc.Text = "Descripción:"
        '
        'LabelSerie
        '
        Me.LabelSerie.AutoSize = True
        Me.LabelSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSerie.Location = New System.Drawing.Point(9, 41)
        Me.LabelSerie.Name = "LabelSerie"
        Me.LabelSerie.Size = New System.Drawing.Size(37, 13)
        Me.LabelSerie.TabIndex = 13
        Me.LabelSerie.Text = "Serie :"
        '
        'LblSerie
        '
        Me.LblSerie.AutoEllipsis = True
        Me.LblSerie.BackColor = System.Drawing.Color.DarkOrange
        Me.LblSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSerie.Location = New System.Drawing.Point(62, 35)
        Me.LblSerie.Name = "LblSerie"
        Me.LblSerie.Size = New System.Drawing.Size(224, 20)
        Me.LblSerie.TabIndex = 3
        Me.LblSerie.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'BSerie
        '
        Me.BSerie.BackColor = System.Drawing.Color.LightGray
        Me.BSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BSerie.Location = New System.Drawing.Point(11, 63)
        Me.BSerie.Name = "BSerie"
        Me.BSerie.Size = New System.Drawing.Size(168, 24)
        Me.BSerie.TabIndex = 43
        '
        'LnlSerie
        '
        Me.LnlSerie.AutoSize = True
        Me.LnlSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LnlSerie.Location = New System.Drawing.Point(9, 47)
        Me.LnlSerie.Name = "LnlSerie"
        Me.LnlSerie.Size = New System.Drawing.Size(49, 15)
        Me.LnlSerie.TabIndex = 42
        Me.LnlSerie.Text = "Serie :"
        '
        'BtnDesc
        '
        Me.BtnDesc.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnDesc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDesc.ForeColor = System.Drawing.Color.Black
        Me.BtnDesc.Location = New System.Drawing.Point(158, 176)
        Me.BtnDesc.Name = "BtnDesc"
        Me.BtnDesc.Size = New System.Drawing.Size(88, 23)
        Me.BtnDesc.TabIndex = 27
        Me.BtnDesc.Text = "&Buscar"
        Me.BtnDesc.UseVisualStyleBackColor = False
        '
        'BtnSerie
        '
        Me.BtnSerie.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnSerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSerie.ForeColor = System.Drawing.Color.Black
        Me.BtnSerie.Location = New System.Drawing.Point(91, 93)
        Me.BtnSerie.Name = "BtnSerie"
        Me.BtnSerie.Size = New System.Drawing.Size(88, 23)
        Me.BtnSerie.TabIndex = 21
        Me.BtnSerie.Text = "&Buscar"
        Me.BtnSerie.UseVisualStyleBackColor = False
        '
        'BDesc
        '
        Me.BDesc.BackColor = System.Drawing.Color.LightGray
        Me.BDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BDesc.Location = New System.Drawing.Point(11, 146)
        Me.BDesc.Name = "BDesc"
        Me.BDesc.Size = New System.Drawing.Size(248, 24)
        Me.BDesc.TabIndex = 26
        '
        'LblDesc
        '
        Me.LblDesc.AutoSize = True
        Me.LblDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDesc.Location = New System.Drawing.Point(12, 128)
        Me.LblDesc.Name = "LblDesc"
        Me.LblDesc.Size = New System.Drawing.Size(87, 15)
        Me.LblDesc.TabIndex = 9
        Me.LblDesc.Text = "Descripción:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(5, 5)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(124, 24)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Por :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_CableModem, Me.Serie, Me.Descripcion})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(543, 570)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'Clv_CableModem
        '
        Me.Clv_CableModem.DataPropertyName = "Clv_CableModem"
        Me.Clv_CableModem.HeaderText = "Id"
        Me.Clv_CableModem.Name = "Clv_CableModem"
        Me.Clv_CableModem.ReadOnly = True
        '
        'Serie
        '
        Me.Serie.DataPropertyName = "Serie"
        Me.Serie.HeaderText = "Serie"
        Me.Serie.Name = "Serie"
        Me.Serie.ReadOnly = True
        Me.Serie.Width = 200
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 200
        '
        'BntSalir
        '
        Me.BntSalir.BackColor = System.Drawing.Color.Orange
        Me.BntSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BntSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BntSalir.ForeColor = System.Drawing.Color.Black
        Me.BntSalir.Location = New System.Drawing.Point(860, 543)
        Me.BntSalir.Name = "BntSalir"
        Me.BntSalir.Size = New System.Drawing.Size(136, 36)
        Me.BntSalir.TabIndex = 11
        Me.BntSalir.Text = "&SALIR"
        Me.BntSalir.UseVisualStyleBackColor = False
        '
        'BtnAgregar
        '
        Me.BtnAgregar.BackColor = System.Drawing.Color.Orange
        Me.BtnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAgregar.ForeColor = System.Drawing.Color.Black
        Me.BtnAgregar.Location = New System.Drawing.Point(860, 12)
        Me.BtnAgregar.Name = "BtnAgregar"
        Me.BtnAgregar.Size = New System.Drawing.Size(136, 36)
        Me.BtnAgregar.TabIndex = 9
        Me.BtnAgregar.Text = "&AGREGAR"
        Me.BtnAgregar.UseVisualStyleBackColor = False
        '
        'LblUserSis
        '
        Me.LblUserSis.AutoSize = True
        Me.LblUserSis.Location = New System.Drawing.Point(903, 31)
        Me.LblUserSis.Name = "LblUserSis"
        Me.LblUserSis.Size = New System.Drawing.Size(39, 13)
        Me.LblUserSis.TabIndex = 10
        Me.LblUserSis.Text = "Label3"
        Me.LblUserSis.Visible = False
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'BrwArtDinercap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 595)
        Me.Controls.Add(Me.BntSalir)
        Me.Controls.Add(Me.BtnAgregar)
        Me.Controls.Add(Me.LblUserSis)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BrwArtDinercap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BrwArtDinercap"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LabelDesc As System.Windows.Forms.Label
    Friend WithEvents LabelSerie As System.Windows.Forms.Label
    Friend WithEvents LblSerie As System.Windows.Forms.Label
    Friend WithEvents BSerie As System.Windows.Forms.TextBox
    Friend WithEvents LnlSerie As System.Windows.Forms.Label
    Friend WithEvents BtnDesc As System.Windows.Forms.Button
    Friend WithEvents BtnSerie As System.Windows.Forms.Button
    Friend WithEvents BDesc As System.Windows.Forms.TextBox
    Friend WithEvents LblDesc As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents LblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Clv_CableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Serie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BntSalir As System.Windows.Forms.Button
    Friend WithEvents BtnAgregar As System.Windows.Forms.Button
    Friend WithEvents LblUserSis As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
