﻿
Imports System.Data.SqlClient

Public Class FrmActualizaTel

    Dim Usuario As String = ""
    Dim fecha As String = ""

    Private Sub FrmActualizaTel_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        colorea(Me)

        Me.Label3.Text = GloContrato
        ConTelCel()

        ConBitacoraActualizaTel()

        If fecha = "" Then
            Label6.Visible = False
        Else
            Label6.Visible = True
        End If

        If Usuario = "" Then
            Label7.Visible = False
        Else
            Label7.Visible = True
        End If

        Me.Show()

        Dim Respuesta As Integer

        Respuesta = MsgBox("Desea Actualizar El Teléfono y/o Celular del cliente", MsgBoxStyle.YesNo, "Atención")
        '6=Yes;7=No
        If Respuesta = 7 Then
            Me.Close()
        End If
    End Sub

    Private Sub ConTelCel()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConTelCel", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloContrato
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Telefono", SqlDbType.VarChar, 100)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Celular", SqlDbType.VarChar, 100)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            TextBox1.Text = parametro1.Value
            TextBox2.Text = parametro2.Value
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConBitacoraActualizaTel()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConBitacoraActualizaTel", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloContrato
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Usuario", SqlDbType.VarChar, 100)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Fecha", SqlDbType.VarChar, 100)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            fecha = parametro2.Value
            Usuario = parametro1.Value
            Label6.Text = "Ultima Actualización: " + fecha
            Label7.Text = "Por el Usuario: " + Usuario
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            If fecha = "" And Usuario = "" Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
                BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBox1.Text, 100)
                BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBox2.Text, 100)
                BaseII.Inserta("ModActualizaTelCliente")

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
                BaseII.CreateMyParameter("@Usuario", SqlDbType.VarChar, GloUsuario, 100)
                BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBox1.Text, 100)
                BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBox2.Text, 100)
                BaseII.Inserta("NueBitacoraActualizaTel")

                MessageBox.Show("Se guardó con éxito.")

                Me.Close()

            Else

                Dim Respuesta2 As Integer

                Respuesta2 = MsgBox("Los datos fueron actualizados el " + fecha + " Por " + Usuario + ", ¿Desea Actualizarlo nuevamente?", MsgBoxStyle.YesNo, "Atención")
                '6=Yes;7=No
                If Respuesta2 = 6 Then
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
                    BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBox1.Text, 100)
                    BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBox2.Text, 100)
                    BaseII.Inserta("ModActualizaTelCliente")

                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
                    BaseII.CreateMyParameter("@Usuario", SqlDbType.VarChar, GloUsuario, 100)
                    BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, TextBox1.Text, 100)
                    BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, TextBox2.Text, 100)
                    BaseII.Inserta("NueBitacoraActualizaTel")

                    MessageBox.Show("Se guardó con éxito.")

                    Me.Close()
                Else
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Label7_Click(sender As System.Object, e As System.EventArgs) Handles Label7.Click

    End Sub

    Private Sub Label6_Click(sender As System.Object, e As System.EventArgs) Handles Label6.Click

    End Sub
End Class