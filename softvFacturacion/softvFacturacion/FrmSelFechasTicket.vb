Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmSelFechasTicket

    Private customersByCityReport As ReportDocument

    'Direccion Sucursal
    Dim RCalleSucur As String = Nothing
    Dim RNumSucur As String = Nothing
    Dim RColSucur As String = Nothing
    Dim RMuniSucur As String = Nothing
    Dim RCiudadSucur As String = Nothing
    Dim RCPSucur As String = Nothing
    Dim RTelSucur As String = Nothing

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub FrmSelFechas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Try
            Me.ComboBox1.DisplayMember = "NOMBRE"
            Me.ComboBox1.ValueMember = "CLV_SUCURSAL"
            BaseII.limpiaParametros()
            Me.ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRASUCURSALES")
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        Me.CMBLabel6.Text = "Sucursal:"
        Me.Text = "Seleccion Fechas Tickets"

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocFecha1 = Me.DateTimePicker1.Text
        LocFecha2 = Me.DateTimePicker2.Text
        'Select Case LocBanderaRep1
        '    Case 0
        '        Locclv_usuario = Me.ComboBox1.SelectedValue
        '        LocNombreusuario = Me.ComboBox1.Text
        '        LocBndrepfac1 = True
        '    Case 1
        '        Locclv_usuario = Me.ComboBox1.SelectedValue
        '        LocNombreusuario = Me.ComboBox1.Text
        '        LocBndrepfac1 = True
        '    Case 2
        '        Locclv_usuario = Me.ComboBox2.SelectedValue
        '        LocNombreusuario = Me.ComboBox2.Text
        '        LocBndBon = True
        'End Select
        ''MsgBox(Locclv_usuario)
        'FrmImprimirRepGral.Show()
        Dim dtTickets As New DataTable
        Dim Clv_Factura As Integer
        Dim X As Integer = 0, Total As Integer = 0

        BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, LocFecha1)
        BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, LocFecha2)
        BaseII.CreateMyParameter("@Sucursal", SqlDbType.Int, ComboBox1.SelectedValue)

        dtTickets = BaseII.ConsultaDT("DameFacturasFechas")

        Total = dtTickets.Rows.Count

        For X = 0 To Total - 1
            Clv_Factura = dtTickets.Rows(X)("Clv_factura").ToString
            ConfigureCrystalReports_tickets(Clv_Factura, "Copia")
        Next X

        'ConfigureCrystalReports_ticketsTodosJuntos("Copia")

        Me.Close()
    End Sub


    Private Sub ConfigureCrystalReports_ticketsTodosJuntos(ByVal oMsj As String)

        customersByCityReport = New ReportDocument

        'MensajesEspeciales(Clv_Factura)

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturasFechas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteCajasTickets_2Varios.rpt"

        Dim parametro As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = LocFecha1
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = LocFecha2
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Sucursal", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = ComboBox1.SelectedValue
        cmd.Parameters.Add(parametro2)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


        da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
        ds.Tables(1).TableName = "CALLES"
        ds.Tables(2).TableName = "CatalogoCajas"
        ds.Tables(3).TableName = "CIUDADES"
        ds.Tables(4).TableName = "CLIENTES"
        ds.Tables(5).TableName = "COLONIAS"
        ds.Tables(6).TableName = "DatosFiscales"
        ds.Tables(7).TableName = "DetFacturas"
        ds.Tables(8).TableName = "DetFacturasImpuestos"
        ds.Tables(9).TableName = "Facturas"
        ds.Tables(10).TableName = "GeneralDesconexion"
        ds.Tables(11).TableName = "SUCURSALES"
        ds.Tables(12).TableName = "Usuarios"
        ds.Tables(13).TableName = "General"

        'DamePerido(GloContrato)
        'DamePeridoTicket(Clv_Factura)
        'If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
        '    GloFechaPeridoPagado = "5"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
        '    GloFechaPeridoPagado = "10"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
        '    GloFechaPeridoPagado = "15"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
        '    GloFechaPeridoPagado = "20"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
        '    GloFechaPeridoPagado = "25"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
        '    GloFechaPeridoPagado = "1"
        'ElseIf GloFechaPeridoPagado = "Periodo : " Then
        '    GloFechaPeridoPagado = " "
        'End If

        'consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'" & oMsj & "'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"
        customersByCityReport.DataDefinition.FormulaFields("Mensaje1").Text = "'" & LocMsj1 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Mensaje2").Text = "'" & LocMsj2 & "'"



        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        customersByCityReport.PrintToPrinter(1, True, 0, 0)

        customersByCityReport.Dispose()

        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long, ByVal oMsj As String)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        MensajesEspeciales(Clv_Factura)

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"


        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = 0
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = "01/01/1900"
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "01/01/1900"
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


        da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
        ds.Tables(1).TableName = "CALLES"
        ds.Tables(2).TableName = "CatalogoCajas"
        ds.Tables(3).TableName = "CIUDADES"
        ds.Tables(4).TableName = "CLIENTES"
        ds.Tables(5).TableName = "COLONIAS"
        ds.Tables(6).TableName = "DatosFiscales"
        ds.Tables(7).TableName = "DetFacturas"
        ds.Tables(8).TableName = "DetFacturasImpuestos"
        ds.Tables(9).TableName = "Facturas"
        ds.Tables(10).TableName = "GeneralDesconexion"
        ds.Tables(11).TableName = "SUCURSALES"
        ds.Tables(12).TableName = "Usuarios"
        ds.Tables(13).TableName = "General"

        'DamePerido(GloContrato)
        'DamePeridoTicket(Clv_Factura)
        If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
            GloFechaPeridoPagado = "5"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
            GloFechaPeridoPagado = "10"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
            GloFechaPeridoPagado = "15"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
            GloFechaPeridoPagado = "20"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
            GloFechaPeridoPagado = "25"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
            GloFechaPeridoPagado = "1"
        ElseIf GloFechaPeridoPagado = "Periodo : " Then
            GloFechaPeridoPagado = " "
        End If

        consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'" & oMsj & "'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"
        customersByCityReport.DataDefinition.FormulaFields("Mensaje1").Text = "'" & LocMsj1 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Mensaje2").Text = "'" & LocMsj2 & "'"


        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        'Dim oPrintLayout As New CrystalDecisions.Shared.PrintLayoutSettings
        'Dim oPrinterSettings As New System.Drawing.Printing.PrinterSettings
        'Dim oPageSettings As New System.Drawing.Printing.PageSettings
        'oPrinterSettings.PrinterName = LocImpresoraTickets
        'oPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("Carta", 850, 1400)
        'oPrintLayout.Scaling = PrintLayoutSettings.PrintScaling.DoNotScale
        'oPrintLayout.Centered = True
        'customersByCityReport.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex
        ''customersByCityReport.PrintOptions.DissociatePageSizeAndPrinterPaperSize = True
        'customersByCityReport.PrintToPrinter(oPrinterSettings, oPageSettings, False, oPrintLayout)
        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        customersByCityReport.Dispose()

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.RCalleSucur = dtDatosGenerales.Rows(0)("calle").ToString
            Me.RNumSucur = dtDatosGenerales.Rows(0)("numero").ToString
            Me.RColSucur = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.RCPSucur = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.RMuniSucur = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.RCiudadSucur = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.RTelSucur = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub

    'Private Sub DamePeridoTicket(ByVal contrato As Long)
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("SP_InformacionTicket", conexion)
    '    comando.CommandType = CommandType.StoredProcedure
    '    comando.CommandTimeout = 0
    '    Dim reader As SqlDataReader

    '    Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
    '    par1.Direction = ParameterDirection.Input
    '    par1.Value = contrato
    '    comando.Parameters.Add(par1)
    '    Dim par2 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
    '    par2.Direction = ParameterDirection.Input
    '    par2.Value = GloClv_Factura
    '    comando.Parameters.Add(par2)

    '    Try
    '        conexion.Open()
    '        reader = comando.ExecuteReader

    '        While (reader.Read())
    '            GloFechaPeridoPagado = reader(0).ToString()
    '            Label26.Text = reader(1).ToString()
    '            GloFechaPeriodoFinal = reader(2).ToString()
    '            GloFechaPeriodoPagadoMes = Label26.Text
    '            GloFechaProximoPago = reader(3).ToString()
    '        End While

    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try
    'End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub
End Class