Imports System.Data.SqlClient
Public Class Form2

    Private Sub CONDETFACTURASBANCOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONDETFACTURASBANCOSBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONDETFACTURASBANCOSBindingSource.EndEdit()
        Me.CONDETFACTURASBANCOSTableAdapter.Connection = CON
        Me.CONDETFACTURASBANCOSTableAdapter.Update(Me.NewsoftvDataSet.CONDETFACTURASBANCOS)
        CON.Close()

    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FillToolStripButton.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONDETFACTURASBANCOSTableAdapter.Connection = CON
            Me.CONDETFACTURASBANCOSTableAdapter.Fill(Me.NewsoftvDataSet.CONDETFACTURASBANCOS, New System.Nullable(Of Long)(CType(CLV_SESSIONBANCOSToolStripTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(OPToolStripTextBox.Text, Integer)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class