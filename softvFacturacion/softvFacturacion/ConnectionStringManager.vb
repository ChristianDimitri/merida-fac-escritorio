﻿Imports System.Configuration
Public Class ConnectionStringManager
    Public Shared Function GetConnectionString(connectionStringName As String) As String
        Dim appconfig As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        Dim connStringSettings As ConnectionStringSettings = appconfig.ConnectionStrings.ConnectionStrings(connectionStringName)
        Return connStringSettings.ConnectionString
    End Function

    Public Shared Sub SaveConnectionString(connectionStringName As String, connectionString As String)
        Dim appconfig As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        appconfig.ConnectionStrings.ConnectionStrings(connectionStringName).ConnectionString = connectionString
        appconfig.Save()
    End Sub

    Public Shared Function GetConnectionStringNames() As List(Of String)
        Dim cns As New List(Of String)()
        Dim appconfig As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        For Each cn As ConnectionStringSettings In appconfig.ConnectionStrings.ConnectionStrings
            cns.Add(cn.Name)
        Next
        Return cns
    End Function

    Public Shared Function GetFirstConnectionStringName() As String
        '       Return GetConnectionStringNames().FirstOrDefault()
    End Function

    Public Shared Function GetFirstConnectionString() As String
        Return GetConnectionString(GetFirstConnectionStringName())
    End Function


End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================

