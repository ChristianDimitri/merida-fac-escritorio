﻿Public Class FrmSelFechaNew

    Private Sub FrmSelFechaNew_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        GloFechaSAP = ""
        DateTimePicker1.Value = Now.Date
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        GloFechaSAP = ""
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        GloFechaSAP = DateTimePicker1.Value.ToShortDateString
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub
End Class